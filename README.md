# README #

Komica Engine - a collection of HTML parsers for different styles that are currently being used by komica.

This project uses DTCoreText to render HTML to NSAttributedString.
https://github.com/Cocoanetics/DTCoreText

And ObjetiveGumbo to parse HTML.
https://github.com/programmingthomas/ObjectiveGumbo
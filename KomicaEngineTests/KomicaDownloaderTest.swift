//
//  KomicaDownloaderTest.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 9/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

import KomicaEngine
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class KomicaDownloaderTest: XCTestCase {
    lazy var animeURL: URL = {
        let url = URL(string: "http://2cat.twbbs.org/~tedc21thc/anime/")
        return url!
    }()
    
    lazy var mangaURL: URL = {
        return URL(string: "http://2cat.or.tl/~scribe/2c/")!
    }()
    
    lazy var mangaResponseURL: URL = {
        return URL(string: "http://2cat.or.tl/~scribe/2c/pixmicat.php?res=356301")!
    }()
    func testDownloadAnime() {
        let expectation = self.expectation(description: "")
        let request = KomicaDownloaderRequest()
        request.url = animeURL
        request.parser = PixmicatListParser.self
        request.completionHandler = {(success: Bool, page: Int, result: ParserResult?)->Void in
            XCTAssert(success)
            XCTAssert(result?.threads?.count > 0)
            expectation.fulfill()
        }
        KomicaDownloader().downloadListForRequest(request)
        self.waitForExpectations(timeout: 120, handler: nil)
    }
    
    func testDownloadMange() {
        let expectation = self.expectation(description: "")
        let request = KomicaDownloaderRequest()
        request.url = mangaURL
        request.parser = PixmicatListParser.self
        request.completionHandler = {(success: Bool, page: Int, result: ParserResult?)->Void in
            XCTAssert(success)
            XCTAssert(result?.threads?.count > 0)
            expectation.fulfill()
        }
        KomicaDownloader().downloadListForRequest(request)
        self.waitForExpectations(timeout: 120, handler: nil)
    }
    
    func testDownloadMangeResponse() {
        let expectation = self.expectation(description: "")
        let request = KomicaDownloaderRequest()
        request.url = mangaResponseURL
        request.parser = PixmicatListParser.self
        request.completionHandler = {(success: Bool, page: Int, result: ParserResult?)->Void in
            XCTAssert(success)
            XCTAssert(result?.threads?.count > 0)
            expectation.fulfill()
        }
        KomicaDownloader().downloadRepliesForRequest(request)
        self.waitForExpectations(timeout: 120, handler: nil)
    }
}

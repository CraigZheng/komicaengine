//
//  ThreadTest.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 7/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

import KomicaEngine
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ThreadTest: XCTestCase {
    lazy var animeHtmlString: String = {
        var htmlString = ""
        htmlString = try! String(contentsOfFile: Bundle(for: type(of: self)).path(forResource: "anime-fragment-1", ofType: "html")!, encoding: String.Encoding.utf8)
        return htmlString
    }()
    
    lazy var overwatchHtmlString: String = {
        var htmlString = ""
        htmlString = try! String(contentsOfFile: Bundle(for: type(of: self)).path(forResource: "overwatch-1", ofType: "html")!, encoding: String.Encoding.utf8)
        return htmlString
    }()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAnimeThread() {
        if let animeElement = ObjectiveGumbo.parseDocument(with: animeHtmlString).elements(withClass: "reply").first as? OGElement {
            print(animeElement.text())
            let thread = PixmicatThreadParser.threadWithOGElement(animeElement)
            XCTAssert(thread != nil)
            XCTAssert(thread?.title == "無念")
            XCTAssert(thread?.content?.string == "請問OP1裡的那女孩裙中，的\"那女孩\"到底是誰?? \n")
            XCTAssert(thread?.ID == "r88457")
            XCTAssert(thread?.UID == "ID:FYhAFZQ2")
            XCTAssert(thread?.postDateString == "16/04/06(水)22:54")
            XCTAssert(thread?.thumbnailURL == URL(string: "http://2cat.twbbs.org/~tedc21thc/anime/thumb/1459954470573s.jpg"))
            XCTAssert(thread?.imageURL == URL(string: "http://2cat.twbbs.org/~tedc21thc/anime/src/1459954470573.jpg"))
            XCTAssert(thread?.warnings.count == 2)
        }
    }
    
    func testOverwatchThread() {
        if let overwatchElement = ObjectiveGumbo.parseDocument(with: overwatchHtmlString).elements(withClass: "threadpost").last as? OGElement {
            print(overwatchElement.text())
            let thread = PixmicatThreadParser.threadWithOGElement(overwatchElement)
            XCTAssert(thread?.videoLinks?.count > 0)
        }
    }
}

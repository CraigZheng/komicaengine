//
//  ListParserTest.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 7/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

import KomicaEngine

class PixmicatListParserTest: XCTestCase {
    lazy var animeHtmlString: String = {
        var htmlString = ""
        htmlString = try! String(contentsOfFile: Bundle(for: type(of: self)).path(forResource: "anime-1", ofType: "html")!, encoding: String.Encoding.utf8)
        return htmlString
    }()
    
    lazy var animeResponseHtmlString: String = {
        var htmlString = ""
        htmlString = try! String(contentsOfFile: Bundle(for: type(of: self)).path(forResource: "anime-responses-1", ofType: "html")!, encoding: String.Encoding.utf8)
        return htmlString
    }()
    
    lazy var mangaHtmlString: String = {
        var htmlString = ""
        htmlString = try! String(contentsOfFile: Bundle(for: type(of: self)).path(forResource: "manga-1", ofType: "html")!, encoding: String.Encoding.utf8)
        return htmlString
    }()
    
    lazy var mangaResponseHtmlString: String = {
        var htmlString = ""
        htmlString = try! String(contentsOfFile: Bundle(for: type(of: self)).path(forResource: "manga-responses-1", ofType: "html")!, encoding: String.Encoding.utf8)
        return htmlString
    }()
    
    lazy var superNaturalHtmlString: String = {
        var htmlString = ""
        htmlString = try! String(contentsOfFile: Bundle(for: type(of: self)).path(forResource: "supernatural-1", ofType: "html")!, encoding: String.Encoding.utf8)
        return htmlString
    }()
    
    lazy var gameNewsHtmlString: String = {
        var htmlString = ""
        htmlString = try! String(contentsOfFile: Bundle(for: type(of: self)).path(forResource: "game-news-1", ofType: "html")!, encoding: String.Encoding.utf8)
        return htmlString
    }()
    
    lazy var hongkongHtmlString: String = {
        var htmlString = ""
        htmlString = try! String(contentsOfFile: Bundle(for: type(of: self)).path(forResource: "hong-kong-1", ofType: "html")!, encoding: String.Encoding.utf8)
        return htmlString
    }()
    
    lazy var gameDiscussionHtmlString: String = {
        var htmlString = ""
        htmlString = try! String(contentsOfFile: Bundle(for: type(of: self)).path(forResource: "game-discussion-1", ofType: "html")!, encoding: String.Encoding.utf8)
        return htmlString
    }()

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testParseHtmlForList() {
        let animeList = PixmicatListParser.listWithHtml(animeHtmlString)
        XCTAssert(animeList?.threads?.count == 10)
        XCTAssert(animeList?.error == nil)
        
        let mangaList = PixmicatListParser.listWithHtml(mangaHtmlString)
        XCTAssert(mangaList?.threads?.count == 6)
    }
    
    func testParseHtmlForReponse() {
        let animeList = PixmicatListParser.repliesWithHtml(animeResponseHtmlString)
        XCTAssert(animeList?.threads?.count == 47)
        
        let mangaList = PixmicatListParser.repliesWithHtml(mangaResponseHtmlString)
        XCTAssert(mangaList?.threads?.count == 24)
    }
    
    func testSupernaturalList() {
        let supernaturalList = PixmicatListParser.listWithHtml(superNaturalHtmlString)
        XCTAssert(supernaturalList?.threads?.count == 10)
        XCTAssert(supernaturalList?.threads?.last?.pushPost?.count == 2)
    }
    
    func testGameNewsList() {
        let gameNewsList = PixmicatListParser.listWithHtml(gameNewsHtmlString)
        XCTAssert(gameNewsList?.threads?.count == 10)
        XCTAssert(gameNewsList?.threads?.last?.pushPost?.count == 2)
    }
    
    func testHongKongList() {
        let gameNewsList = PixmicatListParser.listWithHtml(hongkongHtmlString)
        XCTAssert(gameNewsList?.threads?.count == 15)
    }
    
    func testGameDiscussionList() {
        let gameDiscussion = PixmicatListParser.listWithHtml(gameDiscussionHtmlString)
        XCTAssert(gameDiscussion?.threads?.count == 15)
    }
}

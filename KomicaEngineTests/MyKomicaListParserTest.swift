//
//  MyKomicaListParserTest.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 8/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

import KomicaEngine

class MyKomicaListParserTest: XCTestCase {
    lazy var random2HtmlString: String = {
        var htmlString = ""
        htmlString = try! String(contentsOfFile: Bundle(for: type(of: self)).path(forResource: "random2-1", ofType: "html")!, encoding: String.Encoding.utf8)
        return htmlString
    }()
    
    lazy var random2ResponseHtmlString: String = {
        var htmlString = ""
        htmlString = try! String(contentsOfFile: Bundle(for: type(of: self)).path(forResource: "random2-response-1", ofType: "html")!, encoding: String.Encoding.utf8)
        return htmlString
    }()
    
    func testListWithHtml() {
        let random2List = MyKomicaListParser.listWithHtml(random2HtmlString)
        XCTAssert(random2List?.threads?.count == 8)
    }
    
    func testResponseWithHtml() {
        let random2List = MyKomicaListParser.repliesWithHtml(random2ResponseHtmlString)
        XCTAssert(random2List?.threads?.count == 30)
    }
}

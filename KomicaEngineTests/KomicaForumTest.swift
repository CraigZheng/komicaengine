//
//  KomicaForumTest.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 9/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

import KomicaEngine

class KomicaForumTest: XCTestCase {
    lazy var forumJsonString: String = {
        let jsonString = try! String(contentsOf: Bundle(for: type(of: self)).url(forResource: "Forums", withExtension: "json")!, encoding: String.Encoding.utf8)
        return jsonString
    }()
    
    func testInitWithJson() {
        if let jsonDict = try! JSONSerialization.jsonObject(with: forumJsonString.data(using: String.Encoding.utf8)!, options: .allowFragments) as? Dictionary<String, AnyObject> {
            if let forumsArray = jsonDict["forums"] as? NSArray {
                XCTAssert(forumsArray.count == 5)
                for forumDict in forumsArray {
                    let forum = KomicaForum(jsonDict: (forumDict as! Dictionary<String, AnyObject>))
                    XCTAssert(forum.name != nil)
                    XCTAssert(forum.listURL != nil)
                    XCTAssert(forum.responseURL != nil)
                    XCTAssert(forum.parserType != nil)
                }
            } else {
                XCTAssert(false)
            }
        } else {
            XCTAssert(false)
        }
    }
}

/*
 {
 "name": "二次元動畫＠２ＣＡＴ",
 "header": "",
 "listURL": "http://2cat.twbbs.org/~tedc21thc/anime/<PAGE>.htm?",
 "indexURL": "",
 "responseURL": "http://2cat.twbbs.org/~tedc21thc/anime/pixmicat.php?res=<ID>",
 "postURL": "",
 "parserType": "0"
 }
 */

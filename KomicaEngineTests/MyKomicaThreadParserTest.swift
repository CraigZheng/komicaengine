//
//  MyKomicaThreadParserTest.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 8/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

import KomicaEngine

class MyKomicaThreadParserTest: XCTestCase {
    lazy var htmlString: String = {
        return try! String(contentsOf: Bundle(for: type(of: self)).url(forResource: "random2-fragment-1",
            withExtension: "html")!, encoding: String.Encoding.utf8)
    }()
    
    func testParseThread() {
        if let animeElement = ObjectiveGumbo.parseDocument(with: htmlString).elements(withClass: "threadpost").first as? OGElement {
            print(animeElement.text())
            let thread = MyKomicaThreadParser.threadWithOGElement(animeElement)
            XCTAssert(thread != nil)
            XCTAssert(thread?.title == "1.舞空術")
            XCTAssert(thread?.content?.string == "1.舞空術 \n2.梯雲縱心法 \n3.靈夢會飛 \n")
            XCTAssert(thread?.ID == "r732448")
            XCTAssert(thread?.UID == "ID:JoRE2sqE")
            XCTAssert(thread?.postDateString == "16/04/08 19:40:43")
            XCTAssert(thread?.thumbnailURL == URL(string: "https://imgs.moe/my/alleyneblade/queensblade/thumb/1460115643845s.jpg"))
            XCTAssert(thread?.imageURL == URL(string: "https://imgs.moe/my/alleyneblade/queensblade/src/1460115643845.jpg"))
            XCTAssert(thread?.warnings.count == 0)
        }
    }
}

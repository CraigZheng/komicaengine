//
//  MangaForum.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 9/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

class MangaForum: KomicaForum {
    override init() {
        super.init()
        name = "漫畫"
        listURL = "http://2cat.or.tl/~scribe/2c/"
        parserType = PixmicatListParser.self
        responseURL = "http://2cat.or.tl/~scribe/2c/pixmicat.php?res=<ID>"
    }
    
    override func listURLForPage(_ page: Int) -> URL? {
        var url = listURL
        if page > 5 {
            url = "http://2cat.or.tl/~scribe/2c/pixmicat.php?page_num=\(page)"
        } else if page > 0 {
            url = "http://2cat.or.tl/~scribe/2c/\(page).htm?"
        }
        if let url = url {
            return URL(string: url)
        } else {
            return nil
        }
    }
}

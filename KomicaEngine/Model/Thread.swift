//
//  Thread.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 7/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

open class Thread: NSObject {
    static let greenTextColour = UIColor(red: 121/255.0, green:152/255.0, blue:45/255.0, alpha:1)
    
    @objc open var title: String?
    @objc private(set) open var content: NSAttributedString?
    @objc open var rawHtmlContent: String? {
        didSet {
            let mutableAttributedString = NSMutableAttributedString()
            quotedNumbers.removeAll()
            if let components = rawHtmlContent?.components(separatedBy: CharacterSet.newlines) {
                zip(components.indices, components).forEach({ index, component in
                    // Other than the last component, all should be appended with "\n".
                    var component = component
                    if index != components.indices.last {
                        component.append("\n")
                    }
                    let attributedString: NSAttributedString
                    if component.hasPrefix(">") {
                        attributedString = NSAttributedString(string: component, attributes: [NSForegroundColorAttributeName: Thread.greenTextColour])
                        if let quotedNumber = component.numericValue(),
                            quotedNumber > 0
                        {
                            quotedNumbers.append(quotedNumber)
                        }
                    } else {
                        attributedString = NSAttributedString(string: component)
                    }
                    // Append each segment to the combined attributed string.
                    mutableAttributedString.append(attributedString)
                })
            }
            content = mutableAttributedString
        }
    }
    @objc open var ID: String?
    @objc open var UID: String?
    @objc open var name: String?
    @objc open var email: String?
    @objc open var thumbnailURL: URL?
    @objc open var imageURL: URL?
    @objc open var postDateString: String?
    @objc open var warnings = [String]()

    // Some forums would have push post feature.
    @objc open var pushPost: [String]?
    // Resources, like youtube links etc.
    @objc open var videoLinks: [String]?
    @objc private(set) open var quotedNumbers = [Int]()
}

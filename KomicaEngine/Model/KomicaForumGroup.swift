//
//  KomicaForumGroup.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 16/08/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

@objc open class KomicaForumGroup: NSObject {
    open var name: String?
    open var forums: [KomicaForum]?
    
    public convenience init(jsonDict: Dictionary<String, AnyObject>) {
        self.init()
        
        if let name = jsonDict["name"] as? String {
            self.name = name
        }
        if let forumsDict = jsonDict["forums"] as? Array<Dictionary<String, AnyObject>> {
            forums = [KomicaForum]()
            for jsonDict in forumsDict {
                let forum = KomicaForum(jsonDict: jsonDict)
                forums?.append(forum)
            }
        }
    }

}

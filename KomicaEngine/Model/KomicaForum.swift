//
//  Forum.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 9/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

@objc open class KomicaForum: NSObject {
    // Pixmicat: 0, MyKomica: 1, Siokara: 2, MyMoe: 3, freeStyle: 4, futaba: 5
    @objc open static let parserTypes: [ListParser.Type] = [PixmicatListParser.self, MyKomicaListParser.self, SiokaraListParser.self, MyMoeListParser.self, FreeStyleListParser.self, FutabaListParser.self]
    @objc open static let parserNames: [String] = ["Pixmicat", "My Komica", "Siokara", "My Moe", "Free Style", "Futaba"]
    @objc open var name: String?
    @objc open var header: String? // Short description of this page.
    @objc open var indexURL: String?
    @objc open var startingIndex = 0
    @objc open var listURL: String?
    @objc open var responseURL: String?
    @objc open var postURL: String?
    @objc open var parserType: ListParser.Type?
    open var textEncoding: String.Encoding?
    
    @objc open func listURLForPage(_ page: Int)->URL? {
        var targetURL: URL?
        if let indexURL = indexURL, !indexURL.isEmpty && page == startingIndex {
            targetURL = URL(string: indexURL)
        } else {
            if var listURL = listURL, !listURL.isEmpty {
                listURL = listURL.replacingOccurrences(of: "<PAGE>", with: "\(page)")
                targetURL = URL(string: listURL)
            }
        }
        return targetURL
    }
    
    open func responseURLForThreadID(_ threadID: Int)->URL? {
        if let responseURL = responseURL?.replacingOccurrences(of: "<ID>", with: "\(threadID)"), !responseURL.isEmpty {
            return URL(string: responseURL)
        }
        return nil
    }
    
    public convenience init(jsonDict: Dictionary<String, AnyObject>) {
        self.init()
        
        if let name = jsonDict["name"] as? String {
            self.name = name
        }
        if let header = jsonDict["header"] as? String {
            self.header = header
        }
        if let startingIndex = jsonDict["startingIndex"] as? Int {
            self.startingIndex = startingIndex
        }
        if let urlString = jsonDict["indexURL"] as? String {
            indexURL = urlString
        }
        if let urlString = jsonDict["listURL"] as? String {
            listURL = urlString
        }
        if let urlString = jsonDict["responseURL"] as? String {
            responseURL = urlString
        }
        if let urlString = jsonDict["postURL"] as? String {
            postURL = urlString
        }
        if let typeIndex = jsonDict["parserType"] as? NSNumber {
            if typeIndex.intValue < KomicaForum.parserTypes.count {
                parserType = KomicaForum.parserTypes[typeIndex.intValue]
            } else {
                parserType = PixmicatListParser.self
            }
        }
    }
    
    open func jsonEncode() -> String? {
        let parserIndex = KomicaForum.parserTypes.index(where: { $0 == self.parserType! }) ?? 0
        var jsonDict = Dictionary<String, AnyObject>()
        jsonDict.updateValue(name as AnyObject? ?? "" as AnyObject, forKey: "name")
        jsonDict.updateValue(header as AnyObject? ?? "" as AnyObject, forKey: "header")
        jsonDict.updateValue(indexURL as AnyObject? ?? "" as AnyObject, forKey: "indexURL")
        jsonDict.updateValue(listURL as AnyObject? ?? "" as AnyObject, forKey: "listURL")
        jsonDict.updateValue(responseURL as AnyObject? ?? "" as AnyObject, forKey: "responseURL")
        jsonDict.updateValue(postURL as AnyObject? ?? "" as AnyObject, forKey: "postURL")
        jsonDict.updateValue(NSNumber(value: parserIndex as Int), forKey: "parserType")
        
        if let jsonData = try? JSONSerialization.data(withJSONObject: jsonDict, options: .prettyPrinted) {
            let jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
            return jsonString
        }
        return nil
    }
    
    open override func isEqual(_ object: Any?) -> Bool {
        guard let object = object as? KomicaForum else { return false }
        return indexURL == object.indexURL
        && listURL == object.listURL
        && responseURL == object.responseURL
    }
    
}

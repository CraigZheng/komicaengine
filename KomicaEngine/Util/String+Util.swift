//
//  String+Util.swift
//  KomicaViewer
//
//  Created by Craig Zheng on 14/08/2016.
//  Copyright © 2016 Craig. All rights reserved.
//

import Foundation

extension String {
    
    func numericValue() ->Int? {
        let components = self.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet)
        return Int(components.joinWithSeparator(""))
    }
    
}
//
//  FutabaListParser.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 16/2/17.
//  Copyright © 2017 cz. All rights reserved.
//

import Foundation

open class FutabaListParser: ListParser {
    
    enum ThreadType {
        case lists
        case replies
    }
    
    override open class func listWithHtml(_ htmlString: String, prefferedBaseURLString urlString: String? = nil) -> ParserResult? {
        return threadsWithHtmlString(htmlString, prefferedBaseURLString: urlString, threadType: ThreadType.lists)
    }
    
    override open class func repliesWithHtml(_ htmlString: String, prefferedBaseURLString urlString: String? = nil) -> ParserResult? {
        return threadsWithHtmlString(htmlString, prefferedBaseURLString: urlString, threadType: ThreadType.replies)
    }
    
    fileprivate class func threadsWithHtmlString(_ htmlString: String, prefferedBaseURLString: String? = nil, threadType type: ThreadType) -> ParserResult? {
        var parserResult: ParserResult?
        var dateIPString: [String]?
        
        if let quotes = ObjectiveGumbo.parseDocument(with: htmlString).elements(with: OGTag.blockQuote) as? [OGElement] {
            parserResult = ParserResult()
            parserResult?.threads = [Thread]()
            // Get all date ID strings.
            if dateIPString == nil {
                dateIPString = [String]()
                // Regex pattern for getting the date.
                // [0-9]{2}\/[0-9]{2}\/[0-9]{2}\(.\)[0-9]{2}\:[0-9]{2}\:[0-9]{2} [IiDd]{2}:.+[NnOo]{2}\.[0-9]+
                if let dateRegex = try? NSRegularExpression(pattern: "[/0-9]+\\((.*?)\\)+(.*?) +(.*?) +(.*?) ",
                                                            options: NSRegularExpression.Options.caseInsensitive)
                {
                    // Find name, date, ID and number.
                    dateRegex.enumerateMatches(in: htmlString, options: .reportCompletion, range: NSMakeRange(0, htmlString.characters.count), using: { (result, flag, pointer) in
                        if let result = result {
                            let string = (htmlString as NSString).substring(with: result.range)
                            // Save to an array, to be used later.
                            dateIPString?.append(string)
                        }
                    })
                }
            }
            
            quotes.forEach({ (child) in
                let parentTag = child.parent.tag
                if type == .lists ? parentTag != OGTag.TD : parentTag == OGTag.TD {
                    let newThread = Thread()
                    // Get raw contents, separating it with new line character.
                    let rawContents = child.html().replacingOccurrences(of: "\n", with: "")
                        .replacingOccurrences(of: "<br />", with: "\n")
                        .stringByRemovingHtmlTags
                        .stringByRemovingDuplicateNewLines
                        .components(separatedBy: "\n")
                    newThread.rawHtmlContent = rawContents.joined(separator: "\n")
                    parserResult?.threads?.append(newThread)
                    if let siblingNodes = child.parent.children as? [OGNode] {
                        if let childIndex = siblingNodes.index(of: child) {
                            // Search previous nodes for resource nodes.
                            for i in max(0, childIndex - 15) ..< childIndex {
                                if let previousElement = siblingNodes[i] as? OGElement {
                                    // Nodes for Title, name etc.
                                    if let fontAttributes = previousElement.attributes as? [String: String], previousElement.tag == OGTag.font {
                                        for attribute in fontAttributes {
                                            if attribute.0 == "color" {
                                                if attribute.1 == "#707070" {
                                                    newThread.warnings.append(previousElement.html().stringByRemovingHtmlTags.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
                                                    break
                                                } else if attribute.1 == "#117743" {
                                                    newThread.name = previousElement.html().stringByRemovingHtmlTags.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                                                    break
                                                } else if attribute.1 == "#cc1105" {
                                                    newThread.title = previousElement.html().stringByRemovingHtmlTags.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                                                    break
                                                }
                                            }
                                        }
                                    }
                                    
                                    // Image tags.
                                    // If one of the previous elements has a clickable thumbnail image.
                                    if let aAttributes = previousElement.attributes as? [String: String], previousElement.tag == OGTag.A {
                                        for child in previousElement.children {
                                            if let child = child as? OGElement {
                                                if let imgAttributes = child.attributes as? [String: String], child.tag == OGTag.img {
                                                    // Loop through A tag attribtues, find the one with hyper link.
                                                    for attribute in aAttributes {
                                                        if attribute.0 == "href" {
                                                            newThread.imageURL = URL(string: ("\(prefferedBaseURLString ?? "")\(attribute.1)").httpForm)
                                                            break
                                                        }
                                                    }
                                                    for attribute in imgAttributes {
                                                        if attribute.0 == "src" {
                                                            newThread.thumbnailURL = URL(string: ("\(prefferedBaseURLString ?? "")\(attribute.1)").httpForm)
                                                            break
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    // ID tag, find <input> tags.
                                    if let inputAttributes = previousElement.attributes as? [String: String], previousElement.tag == OGTag.input {
                                        for attribute in inputAttributes {
                                            if attribute.0 == "name" {
                                                newThread.ID = attribute.1
                                                break
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            })
        }
        for thread in parserResult?.threads ?? [] {
            for dateString in dateIPString ?? [] {
                if let ID = thread.ID, (dateString as NSString).range(of: ID).location != NSNotFound
                {
                    let components = dateString.components(separatedBy: " ")
                    if components.count >= 2 {
                        thread.postDateString = components[0]
                        thread.UID = components[1]
                    }
                }
            }
        }
        return parserResult
    }
    
    override class func threadParser() -> ThreadParser.Type {
        return SiokaraThreadParser.self
    }
    
}

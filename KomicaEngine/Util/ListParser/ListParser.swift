//
//  ListParser.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 7/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

//import ObjectiveGumbo

@objc open class ParserResult: NSObject {
    open var threads: [Thread]?
    open var error: NSError?
}

open class ListParser: NSObject {

    @objc open class func listWithHtml(_ htmlString: String, prefferedBaseURLString: String? = nil)->ParserResult? {
        print(#function + " not implemented!")
        return nil
    }
    
    @objc open class func repliesWithHtml(_ htmlString: String, prefferedBaseURLString: String? = nil)->ParserResult? {
        print(#function + " not implemented!")
        return nil
    }
    
    class func threadsWithOGElements(_ elements: [OGElement], prefferedBaseURLString: String? = nil)->ParserResult? {
        print(#function + " not implemented!")
        return nil
    }
    
    class func threadParser()->ThreadParser.Type {
        return ThreadParser.self
    }

}

//
//  MyMoeListParser.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 23/08/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

class MyMoeListParser: MyKomicaListParser {
    // The thread parser type would be MyKomicaThreadParser.
    override class func threadParser()->ThreadParser.Type {
        return MyMoeThreadParser.self
    }
    
    override class func listWithHtml(_ htmlString:String, prefferedBaseURLString: String? = nil) -> ParserResult? {
        let result = super.listWithHtml(htmlString)
        if let result = result {
            processVideoLinksWith(result, and: htmlString)
        }
        return result
    }
    
    override class func repliesWithHtml(_ htmlString:String, prefferedBaseURLString: String? = nil) -> ParserResult? {
        let result = super.repliesWithHtml(htmlString)
        if let result = result {
            processVideoLinksWith(result, and: htmlString)
        }
        return result
    }
    
    fileprivate class func processVideoLinksWith(_ parserResult: ParserResult, and htmlString: String) {
        if let threads = parserResult.threads, !threads.isEmpty {
            // Grab video links and match each with cooresponding thread object.
            let endTag = "</script>"
            if !htmlString.isEmpty {
                if let document = ObjectiveGumbo.parseDocument(with: htmlString),
                    let scriptElements = document.elements(with: OGTag.script) as? [OGElement] {
                    scriptElements.forEach({ (element) in
                        let elementHtml = element.html()
                        if elementHtml?.contains("youtube") ?? false,
                            let components = elementHtml?.components(separatedBy: "="),
                            components.count == 2,
                            let playerID = components.first?.numericValue(),
                            let jsonData = (components.last as NSString?)?.replacingOccurrences(of: endTag, with: "")
                                .replacingOccurrences(of: ";", with: "")
                                .data(using: .utf8),
                            let youtubeJsonDict = ((try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)) as? [[String: String]])?.first,
                            let youtubeString = youtubeJsonDict["url"] {
                            threads.forEach({ (thread) in
                                if let threadID = thread.ID?.numericValue(),
                                    threadID == playerID {
                                    if thread.videoLinks == nil {
                                        thread.videoLinks = [String]()
                                    }
                                    thread.videoLinks?.append(youtubeString)
                                }
                            })
                        }
                    })
                }
            }
        }
    }
}

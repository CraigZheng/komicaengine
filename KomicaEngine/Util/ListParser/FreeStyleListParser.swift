//
//  FreeStyle.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 25/08/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

import DTCoreText

class FreeStyleListParser: ListParser {

    override class func listWithHtml(_ htmlString: String, prefferedBaseURLString: String? = nil)->ParserResult? {
        var result: ParserResult?
        var threads = [Thread]()
        if let inputs = ObjectiveGumbo.parseDocument(with: htmlString).elements(with: OGTag.input) as? [OGElement] {
            result = ParserResult()
            inputs.forEach({input in
                if let attributes = input.attributes as? Dictionary<String, String> {
                    //<input name="key" value="1450882360" type="hidden">
                    if attributes["name"] == "key" && attributes["type"] == "hidden" {
                        let parent = input.parent
                        let newThread = Thread()
                        newThread.ID = attributes["value"]
                        // Find blockquotes under the parent node.
                        for blockquote in (parent?.elements(with: OGTag.blockQuote))! {
                            if let blockquote = blockquote as? OGElement {
                                newThread.rawHtmlContent = blockquote.html()
                            }
                        }
                        for trNode in (parent?.elements(with: OGTag.TR))! {
                            if let trNode = trNode as? OGElement {
                                if let attributes = trNode.attributes as? Dictionary<String, String> {
                                    if let colour = attributes["bgcolor"] {
                                        switch colour {
                                        case "94B4DA":
                                            // Date and poster.
                                            let rawText = trNode.text().trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                                            // Use regex to get date.
                                            // Regex: [0-9]{4}\/[0-9]{2}\/[0-9]{2}\(.+\) [0-9]{2}:[0-9]{2}
                                            if let dateTimeRegex = try? NSRegularExpression(pattern: "[0-9]{4}\\/[0-9]{2}\\/[0-9]{2}\\(.+\\) [0-9]{2}:[0-9]{2}", options: .caseInsensitive) {
                                                dateTimeRegex.enumerateMatches(in: rawText, options: .reportCompletion, range: NSMakeRange(0, rawText.characters.count), using: { (result, flag, pointer) in
                                                    if let result = result {
                                                        let dateString = (rawText as NSString).substring(with: result.range)
                                                        // Save to the current new thread as date.
                                                        newThread.postDateString = dateString
                                                        // Remove dateString from the original rawText, use it as the poster.
                                                        let poster = rawText.replacingOccurrences(of: dateString, with: "")
                                                        newThread.name = poster
                                                    }
                                                })
                                            }
                                            // Regex for name: IP+.+\]
                                            if let nameRegex = try? NSRegularExpression(pattern: "IP+.+\\*", options: .caseInsensitive) {
                                                nameRegex.enumerateMatches(in: rawText, options: .reportCompletion, range: NSMakeRange(0, rawText.characters.count), using: { (result, flag, pointer) in
                                                    if let result = result {
                                                        let nameString = (rawText as NSString).substring(with: result.range)
                                                        newThread.UID = nameString
                                                    }
                                                })
                                            }
                                            break
                                        case "CEDFEF":
                                            // Title.
                                            newThread.title = trNode.text().trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                                        default: break
                                        }
                                    }
                                }
                            }
                        }
                        threads.append(newThread)
                    }
                }
            })
        }
        result?.threads = threads
        return result
    }
    
    override class func repliesWithHtml(_ htmlString: String, prefferedBaseURLString: String? = nil)->ParserResult? {
        var result: ParserResult?
        if let ddElements = ObjectiveGumbo.parseDocument(with: htmlString).elements(with: OGTag.dd) as? [OGElement],
            let dtElements = ObjectiveGumbo.parseDocument(with: htmlString).elements(with: OGTag.dt) as? [OGElement]
        {
            let count = min(ddElements.count, dtElements.count)
            result = ParserResult()
            result?.threads = [Thread]()
            // Use Regex to get the date from dt element: [0-9]{4}\/[0-9]{2}\/+.+[0-9]{2}:[0-9]{2}
            // IP regex: IP+.+\*
            for i in 1 ..< count {
                if let dateTimeRegex = try? NSRegularExpression(pattern: "[0-9]{4}\\/[0-9]{2}\\/+.+[0-9]{2}:[0-9]{2}", options: .caseInsensitive),
                    let nameRegex = try? NSRegularExpression(pattern: "IP+.+\\*", options: .caseInsensitive)
                {
                    let rawTitle = dtElements[i].text()
                    let newThread = Thread()
                    result?.threads?.append(newThread)
                    // Assign values.
                    dateTimeRegex.enumerateMatches(in: rawTitle!, options: .reportCompletion, range: NSMakeRange(0, (rawTitle?.characters.count)!), using: { (result, flag, pointer) in
                        if let result = result, let rawTitle = rawTitle {
                            let dateTimeString = (rawTitle as NSString).substring(with: result.range)
                            newThread.postDateString = dateTimeString
                        }
                    })
                    nameRegex.enumerateMatches(in: rawTitle!, options: .reportCompletion, range: NSMakeRange(0, (rawTitle?.characters.count)!), using: { (result, flag, pointer) in
                        if let result = result, let rawTitle = rawTitle {
                            let nameString = (rawTitle as NSString).substring(with: result.range)
                            newThread.UID = nameString
                        }
                    })
                    newThread.ID = rawTitle?.components(separatedBy: " ").first
                    newThread.rawHtmlContent = ddElements[i].html().replacingOccurrences(of: "\n", with: "")
                        .replacingOccurrences(of: "<br />", with: "\n")
                        .stringByRemovingHtmlTags
                        .stringByRemovingDuplicateNewLines
                        .trimmingCharacters(in: CharacterSet.newlines)
                }
            }
        }
        return result
    }
}

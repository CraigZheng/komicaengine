//
//  FutabaParser.swift
//  KomicaEngine
//
//  Created by Craig on 8/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

//import ObjectiveGumbo

// Structure is similar to pixmicat.
open class MyKomicaListParser: PixmicatListParser {
    
    // The thread parser type would be MyKomicaThreadParser.
    override class func threadParser()->ThreadParser.Type {
        return MyKomicaThreadParser.self
    }
    
}

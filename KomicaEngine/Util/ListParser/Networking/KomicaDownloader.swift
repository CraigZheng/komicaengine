//
//  KomicaDownloader.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 9/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

public typealias KomicaDownloaderHandler = (_ success: Bool, _ page: Int, _ result: ParserResult?)->Void

@objc open class KomicaDownloaderRequest: NSObject {
    
    @objc open var url: URL!
    @objc open var page: Int = 0
    @objc open var parser: ListParser.Type!
    open var preferredEncoding: String.Encoding?
    @objc open var preferredBaseURLString: String?
    @objc open var completionHandler: KomicaDownloaderHandler!
    
    public convenience init(url: URL!, page: Int, parser: ListParser.Type!, completion: KomicaDownloaderHandler!) {
        self.init()
        self.url = url
        self.page = page
        self.parser = parser
        self.completionHandler = completion
    }
}

open class KomicaDownloader: NSObject {

    fileprivate let defaultSession = URLSession(configuration: .default)
    
    /**
     Download a list of threads from the given URL, you typically pass a URL to a forum here.
     @return: NSURLSessionTask object that's been created.
     */
    @objc open func downloadListForRequest(_ request: KomicaDownloaderRequest)->URLSessionTask? {
        let sessionTask = defaultSession.dataTask(with: URLRequest(url: request.url), completionHandler: { (responseObject, response, error) in
            let encoding = self.encodingFor(request, responseObject)
            if error == nil,
                let data = responseObject,
                let htmlString = String(data: data, encoding: encoding) {
                DispatchQueue.main.async(execute: {
                    let result = request.parser.listWithHtml(htmlString, prefferedBaseURLString: request.preferredBaseURLString)
                    request.completionHandler(true, request.page, result)
                })
            } else {
                request.completionHandler(false, request.page, nil)
            }
        }) 
        sessionTask.resume()
        return sessionTask
    }
    
    /**
     Download a list of repiles from the given URL, you typically pass a URL to a thread here.
     */
    @objc open func downloadRepliesForRequest(_ request: KomicaDownloaderRequest)->URLSessionTask? {
        let sessionTask = defaultSession.dataTask(with: URLRequest(url: request.url), completionHandler: { (responseObject, response, error) in
            let encoding = self.encodingFor(request, responseObject)
            if error == nil,
                let data = responseObject,
                let htmlString = String(data: data, encoding: encoding) {
                DispatchQueue.main.async(execute: {
                    let result = request.parser.repliesWithHtml(htmlString, prefferedBaseURLString: request.preferredBaseURLString)
                    request.completionHandler(true, request.page, result)
                })
            } else {
                request.completionHandler(false, request.page, nil)
            }
        }) 
        sessionTask.resume()
        return sessionTask
    }
    
    fileprivate func encodingFor(_ request: KomicaDownloaderRequest, _ htmlData: Data?) -> String.Encoding {
        var encoding = String.Encoding.utf8
        if let prefferedEncoding = request.preferredEncoding {
            // When a preffered encoding has been specified, use that.
            encoding = prefferedEncoding
        } else if let data = htmlData {
            // Guess an encoding.
            let encodingRaw = NSString.stringEncoding(for: data,
                                                      encodingOptions: [.suggestedEncodingsKey: [String.Encoding.utf8.rawValue,
                                                                                                 String.Encoding.shiftJIS.rawValue,
                                                                                                 String.Encoding.unicode.rawValue]],
                                                      convertedString: nil,
                                                      usedLossyConversion: nil)
            if encodingRaw > 0 {
                let tempEncoding = String.Encoding(rawValue: NSString.stringEncoding(for: data,
                                                                                     encodingOptions: [.suggestedEncodingsKey: [String.Encoding.utf8.rawValue,
                                                                                                                                String.Encoding.shiftJIS.rawValue,
                                                                                                                                String.Encoding.unicode.rawValue]],
                                                                                     convertedString: nil,
                                                                                     usedLossyConversion: nil))
                encoding = tempEncoding
            }
        }
        return encoding
    }
}

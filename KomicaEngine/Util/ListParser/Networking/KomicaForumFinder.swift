//
//  KomicaPageFinder.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 9/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

/**
 This singelton class would help you find the URL for any page you like.
 */

@objc open class KomicaForumFinder: NSObject {

    fileprivate let updateURL = URL(string: "http://civ.atwebpages.com/KomicaEngine/komica_engine_remote_forums.php")!
    fileprivate let defaultSession = URLSession(configuration: .default)
    
    @objc open static let sharedInstance = KomicaForumFinder()
    @objc open lazy var forumGroups: [KomicaForumGroup] = {
        var forumGroups = [KomicaForumGroup]()
        let jsonString = try! String(contentsOf: Bundle(for: type(of: self)).url(forResource: "Forums", withExtension: "json")!, encoding: String.Encoding.utf8)
        if let jsonData = jsonString.data(using: String.Encoding.utf8),
            let parsedGroup = KomicaForumFinder.parseJSONData(jsonData) {
            forumGroups = parsedGroup
        }
        
        return forumGroups
    }()
    
    fileprivate class func parseJSONData(_ jsonData: Data) -> [KomicaForumGroup]? {
        var forumGroups: [KomicaForumGroup]?
        if let jsonDict = (try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)) as? Dictionary<String, AnyObject> {
            if let sections = jsonDict["sections"] as? NSArray {
                forumGroups = [KomicaForumGroup]()
                for sectionDict in sections {
                    if let sectionDict = sectionDict as? Dictionary<String, AnyObject> {
                        let forumGroup = KomicaForumGroup(jsonDict: sectionDict)
                        forumGroups?.append(forumGroup)
                    }
                }
            }
        }
        return forumGroups
    }
    
    /**
     Unique forum that cannot be represented by JSON file, each have their own set of rules.
     */
    @objc open lazy var uniqueForums: [KomicaForum] = {
        var forums = [KomicaForum]()
        forums.append(MangaForum())
        // TODO: might be more in the future.
        return forums
    }()
    
    open func loadRemoteForumsWithCompletion(_ completion: ((_ success: Bool, _ remoteFroumGroups: [KomicaForumGroup]?, _ error: NSError?)->())?) {
        loadRemoteForumsFrom(url: updateURL, withCompletion: completion)
    }
    
    open func loadRemoteForumsFrom(url: URL, withCompletion completion: ((_ success: Bool, _ remoteFroumGroups: [KomicaForumGroup]?, _ error: NSError?)->())?) {
        defaultSession.dataTask(with: URLRequest(url: url), completionHandler: {(responseObject, response, error) in
            var success = false
            var remoteForumGroupss: [KomicaForumGroup]?
            if let jsonData = responseObject {
                let parsedGroup = KomicaForumFinder.parseJSONData(jsonData)
                remoteForumGroupss = parsedGroup
                success = true
            }
            completion?(success, remoteForumGroupss, error as NSError?)
        }).resume()
    }

}

//
//  PiximicatParser.swift
//  KomicaEngine
//
//  Created by Craig on 8/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

//import ObjectiveGumbo

@objc open class PixmicatListParser: ListParser {
    override open class func listWithHtml(_ htmlString: String, prefferedBaseURLString: String? = nil)->ParserResult? {
        if let threadElements = ObjectiveGumbo.parseDocument(with: htmlString).elements(withClass: "threadpost") as? [OGElement] {
            return threadsWithOGElements(threadElements, prefferedBaseURLString: prefferedBaseURLString)
        } else {
            // TODO: has no threadpost, need a better error.
            return nil
        }
    }
    
    override open class func repliesWithHtml(_ htmlString: String, prefferedBaseURLString: String? = nil)->ParserResult? {
        if let threadElements = ObjectiveGumbo.parseDocument(with: htmlString).elements(withClass: "reply") as? [OGElement] {
            return threadsWithOGElements(threadElements, prefferedBaseURLString: prefferedBaseURLString)
        } else {
            // TODO: has no threadpost, need a better error.
            return nil
        }
    }
    
    override class func threadsWithOGElements(_ elements: [OGElement], prefferedBaseURLString: String? = nil)->ParserResult? {
        var threads = [Thread]()
        let error: NSError? = nil
        for (index, element) in elements.enumerated() {
            if let thread = threadParser().threadWithOGElement(element, prefferedBaseURLString: prefferedBaseURLString) {
                threads.append(thread)
            }
        }
        // TODO: a more suitable error.
        let result = ParserResult()
        result.threads = threads
        result.error = error
        return result
    }
    
    // Override to provide a parser type.
    override class func threadParser()->ThreadParser.Type {
        return PixmicatThreadParser.self
    }
}

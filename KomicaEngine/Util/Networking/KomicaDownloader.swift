//
//  KomicaDownloader.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 9/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

import AFNetworking

@objc public class KomicaDownloaderRequest: NSObject {
    @objc public var url: NSURL!
    @objc public var parser: ListParser.Type!
    @objc public var completionHandler: ((success: Bool, result: ParserResult?)->Void)!
}

public class KomicaDownloader: NSObject {

    lazy var sessionManager: AFHTTPSessionManager = {
        let sessionManager = AFHTTPSessionManager()
        sessionManager.responseSerializer = AFHTTPResponseSerializer()
        return sessionManager
    } ()
    
    /**
     Download a list of threads from the given URL, you typically pass a URL to a forum here.
     */
    @objc public func downloadListForRequest(request: KomicaDownloaderRequest) {
        sessionManager.dataTaskWithRequest(NSURLRequest(URL: request.url)) { (response, responseObject, error) in
            if error == nil,
                let data = responseObject as? NSData,
                let htmlString = String(data: data, encoding: NSUTF8StringEncoding) {
                request.completionHandler(success: true, result: request.parser.listWithHtml(htmlString))
            } else {
                request.completionHandler(success: false, result: nil)
            }
            }.resume()
    }
    
    /**
     Download a list of repiles from the given URL, you typically pass a URL to a thread here.
     */
    @objc public func downloadRepliesForRequest(request: KomicaDownloaderRequest) {
        sessionManager.dataTaskWithRequest(NSURLRequest(URL: request.url)) { (response, responseObject, error) in
            if error == nil,
                let data = responseObject as? NSData,
                let htmlString = String(data: data, encoding: NSUTF8StringEncoding) {
                request.completionHandler(success: true, result: request.parser.repliesWithHtml(htmlString))
            } else {
                request.completionHandler(success: false, result: nil)
            }
            }.resume()
    }
}
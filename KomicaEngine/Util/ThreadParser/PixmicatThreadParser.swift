//
//  PixmicatThreadParser.swift
//  KomicaEngine
//
//  Created by Craig on 8/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

//import ObjectiveGumbo
import DTCoreText

open class PixmicatThreadParser: ThreadParser {
    open override class func threadWithOGElement(_ element: OGElement, prefferedBaseURLString: String? = nil)->Thread? {
        let thread = Thread()
        let rawText = element.text()
        let rawHtml = element.html()
        // ID
        if let ID = element.attributes["id"] as? String{
            thread.ID = ID
        }
        // Titie
        if let titleElement = element.elements(withClass: "title").first as? OGElement {
            thread.title = titleElement.text()
        }
        // Content - if there're push post, add them, and set quoteElement to be the parentNode for pushpost element..
        var quoteElement = element.elements(withClass: "quote").first
        if let pushPostElement = element.elements(withClass: "pushpost").first as? OGElement {
            let quoteHtmlWithoutPushPost = pushPostElement.parent.html(withIndentation: 1)
                .trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                .replacingOccurrences(of: pushPostElement.html(withIndentation: 2)    .trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), with: "")
            quoteElement = ObjectiveGumbo.parseNode(with: quoteHtmlWithoutPushPost)
            // Seperate pushpost content and assign each as an individual string.
            thread.pushPost = [String]()
            for line in pushPostElement.html().components(separatedBy: "<br />") {
                let string = line.stringByRemovingHtmlTags.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).components(separatedBy: CharacterSet.newlines).joined(separator: "")
                thread.pushPost?.append(string)
            }
        }
        
        if let quoteElement = quoteElement as? OGElement {
            // Use DTCoreText, because there is an issue with the NSAttributedString for the retaining.
            var attributedString: NSAttributedString?
            var htmlString = quoteElement.html()
            if let data = htmlString?.data(using: String.Encoding.utf8) {
                attributedString = NSAttributedString(htmlData: data, options: [DTUseiOS6Attributes: false], documentAttributes: nil)
            }
            htmlString = attributedString?.string
            if (htmlString ?? "").hasSuffix("\n") {
                htmlString = htmlString?.trimmingCharacters(in: CharacterSet.newlines)
            }
            thread.rawHtmlContent = htmlString
        }
        // Email
        for element in element.elements(with: OGTag.A) {
            if let element = element as? OGElement {
                if let emailString = element.attributes["href"] as? String {
                    if emailString.contains("mailto") {
                        thread.email = emailString
                    }
                }
            }
        }
        // UID and post date time.
        if let rawText = rawText,
            let idRegex = try? NSRegularExpression(pattern: "\\ ID(.*?)\\ ", options: .caseInsensitive),
            let dateRegex = try? NSRegularExpression(pattern: "[/0-9]+\\((.*?)\\)+(.*?)\\ ", options: .caseInsensitive)
        {
            // UID.
            for match in idRegex.matches(in: rawText, options: [], range: NSMakeRange(0, rawText.characters.count)) {
                if let swiftRange = rawText.range(from: match.range) {
                    if rawText.substring(with: swiftRange).contains("ID") {
                        thread.UID = rawText.substring(with: swiftRange)
                        break
                    }
                }
            }
            // Post date.
            for match in dateRegex.matches(in: rawText, options: [], range: NSMakeRange(0, rawText.characters.count)) {
                if let swiftRange = rawText.range(from: match.range) {
                    thread.postDateString = rawText.substring(with: swiftRange)
                    break
                }
            }
        }
        // Image URLs.
        if let thumbnailElement = element.elements(with: OGTag.img).first as? OGElement,
            let thumbnailSrc = thumbnailElement.attributes["src"] as? String {
            if let thumbnailURL = URL(string: thumbnailSrc)?.by(injecting: prefferedBaseURLString) {
                var components = URLComponents(url: thumbnailURL, resolvingAgainstBaseURL: false)
                if components?.scheme?.isEmpty ?? false {
                    components?.scheme = "http"
                }
                thread.thumbnailURL = components?.url;
            }
            if let parent = thumbnailElement.parent,
                let imageSrc = parent.attributes["href"] as? String,
                let imageURL = URL(string: imageSrc)?.by(injecting: prefferedBaseURLString) {
                var components = URLComponents(url: imageURL, resolvingAgainstBaseURL: false)
                if components?.scheme?.isEmpty ?? false {
                    components?.scheme = "http"
                }
                thread.imageURL = components?.url
            }
        }
        // Warnings.
        for spanElement in element.elements(with: OGTag.span) {
            if let spanElement = spanElement as? OGElement {
                if let warnText = spanElement.attributes["class"] as? String {
                    if warnText.contains("warn_txt") {
                        thread.warnings.append(spanElement.text())
                    }
                }
            }
        }
        // Resources.
        // Regex for youtube link is ytclick\((.*?)\)
        // Regex for anything within double or single quote: [\'\"](.*?)[\'\"]
        if let youtubeRegex = try? NSRegularExpression(pattern: "ytclick\\((.*?)\\)", options: .caseInsensitive),
            let quoteRegex = try? NSRegularExpression(pattern: "[\'](.*?)[\']", options: .caseInsensitive)
        {
            youtubeRegex.enumerateMatches(in: rawHtml!,
                                          options: .reportCompletion,
                                          range: NSMakeRange(0, (rawHtml?.characters.count)!),
                                          using: { (result, flag, stop) in
                                            if let result = result, result.range.location + result.range.length < (rawHtml?.characters.count)!,
                                                let rawHtml = rawHtml
                                            {
                                                let resultString = (rawHtml as NSString).substring(with: result.range)
                                                quoteRegex.enumerateMatches(in: resultString,
                                                                            options: .reportCompletion,
                                                                            range: NSMakeRange(0, resultString.characters.count),
                                                                            using: { (result, flag, stop) in
                                                                                // The first is the youtube token, you can stop now.
                                                                                if thread.videoLinks == nil {
                                                                                    thread.videoLinks = [String]()
                                                                                    // https://www.youtube.com/watch?v=x0aQ9DXLG1Q
                                                                                    if let result = result, !(resultString as NSString).substring(with: result.range).isEmpty
                                                                                    {
                                                                                        var youtubeToken = (resultString as NSString).substring(with: result.range)
                                                                                        if youtubeToken.characters.count > 3 {
                                                                                            // Substring without the first and the last characters.
                                                                                            youtubeToken = (youtubeToken as NSString).substring(with: NSMakeRange(1, youtubeToken.characters.count - 2))
                                                                                            thread.videoLinks?.append("https://www.youtube.com/watch?v=\(youtubeToken)")
                                                                                        }
                                                                                    }
                                                                                }
                                                })
                                            }
            })
        }
        return thread
    }
}

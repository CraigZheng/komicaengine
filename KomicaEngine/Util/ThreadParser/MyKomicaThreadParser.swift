//
//  FutabaThreadParser.swift
//  KomicaEngine
//
//  Created by Craig on 8/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

//import ObjectiveGumbo

open class MyKomicaThreadParser: PixmicatThreadParser {
    open override class func threadWithOGElement(_ element: OGElement, prefferedBaseURLString: String? = nil)->Thread? {
        let thread = super.threadWithOGElement(element, prefferedBaseURLString: prefferedBaseURLString)
        thread?.postDateString = (element.elements(with: OGTag.time).first as? OGElement)?.text()
        thread?.UID = (element.elements(withClass: "trip_id").first as? OGElement)?.text()
        return thread
    }
}

//
//  MyMoeThreadParser.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 23/08/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

class MyMoeThreadParser: MyKomicaThreadParser {
    
    open override class func threadWithOGElement(_ element: OGElement, prefferedBaseURLString: String? = nil) -> Thread? {
        let thread = super.threadWithOGElement(element, prefferedBaseURLString: prefferedBaseURLString)
        
        // Get MyMoe specific videos.
        if let youtubeRegex = try? NSRegularExpression(pattern: "<script>(.*?)<\\/script>", options: .caseInsensitive),
            let rawHtml = element.html(),
            let thread = thread {
            youtubeRegex.enumerateMatches(in: rawHtml,
                                          options: .reportCompletion,
                                          range: NSMakeRange(0, rawHtml.characters.count),
                                          using: { (result, flag, stop) in
                                            if let result = result, result.range.location + result.range.length < rawHtml.characters.count {
                                                let resultString = (rawHtml as NSString).substring(with: result.range)
                                                let components = resultString.components(separatedBy: "=")
                                                if resultString.contains("youtube"), components.count == 2 {
                                                    if thread.videoLinks == nil {
                                                        thread.videoLinks = [String]()
                                                        // https://www.youtube.com/watch?v=x0aQ9DXLG1Q
                                                        if !(resultString as NSString).substring(with: result.range).isEmpty {
                                                            var youtubeToken = (resultString as NSString).substring(with: result.range)
                                                            if youtubeToken.characters.count > 3 {
                                                                // Substring without the first and the last characters.
                                                                youtubeToken = (youtubeToken as NSString).substring(with: NSMakeRange(1, youtubeToken.characters.count - 2))
                                                                thread.videoLinks?.append("https://www.youtube.com/watch?v=\(youtubeToken)")
                                                            }
                                                        }
                                                    }
                                                }
                                            }
            })
        }
        return thread
    }

}

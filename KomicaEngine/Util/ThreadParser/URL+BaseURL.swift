//
//  URL+BaseURL.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 8/2/18.
//  Copyright © 2018 cz. All rights reserved.
//

import Foundation

extension URL {
    func by(injecting baseURLString: String?) -> URL {
        guard let baseURLString = baseURLString,
            let originalURLComponents = URLComponents(url: self, resolvingAgainstBaseURL: false),
            var newURLComponents = URLComponents(string: baseURLString) else {
                return self
        }
        if let originalScheme = originalURLComponents.scheme {
            newURLComponents.scheme = originalScheme
        }
        if let originalHost = originalURLComponents.host {
            newURLComponents.host = originalHost
        }
        newURLComponents.path = originalURLComponents.path
        return newURLComponents.url ?? self
    }
}

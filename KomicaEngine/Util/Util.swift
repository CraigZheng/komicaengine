//
//  Util.swift
//  KomicaEngine
//
//  Created by Craig Zheng on 7/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation
import UIKit

class KomicaEngineUtil {
    
//    /*
//     Copied from https://www.hackingwithswift.com/example-code/strings/nsregularexpression-how-to-match-regular-expressions-in-strings
//     */
//    class func swiftRangeFromNSRange(_ nsRange: NSRange, forString str: String) -> Range<String.Index>? {
//        guard let from16 = utf16.index(utf16.startIndex, offsetBy: nsRange.location, limitedBy: utf16.endIndex),
//            let to16 = utf16.index(from16, offsetBy: nsRange.length, limitedBy: utf16.endIndex),
//            let from = String.Index(from16, within: self),
//            let to = String.Index(to16, within: self)
//            else { return nil }
//        return from ..< to
//    }
//    
}

extension String {
    func nsRange(from range: Range<String.Index>) -> NSRange {
        let from = range.lowerBound.samePosition(in: utf16)
        let to = range.upperBound.samePosition(in: utf16)
        return NSRange(location: utf16.distance(from: utf16.startIndex, to: from),
                       length: utf16.distance(from: from, to: to))
    }
}

extension String {
    func range(from nsRange: NSRange) -> Range<String.Index>? {
        guard
            let from16 = utf16.index(utf16.startIndex, offsetBy: nsRange.location, limitedBy: utf16.endIndex),
            let to16 = utf16.index(from16, offsetBy: nsRange.length, limitedBy: utf16.endIndex),
            let from = String.Index(from16, within: self),
            let to = String.Index(to16, within: self)
            else { return nil }
        return from ..< to
    }
}

//
//  String+Util.swift
//  KomicaViewer
//
//  Created by Craig Zheng on 14/08/2016.
//  Copyright © 2016 Craig. All rights reserved.
//

import Foundation

extension String {
    
    public func numericValue() ->Int? {
        let components = self.components(separatedBy: CharacterSet.decimalDigits.inverted)
        return Int(components.joined(separator: ""))
    }
    
}

extension String {
    
    var decodeHtml: NSAttributedString? {
        guard
            let data = data(using: String.Encoding.utf8)
            else { return nil }
        do {
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }

}

extension String {
    var stringByRemovingHtmlTags: String {
        var r: NSRange
        var s = NSString(string: self)
        r = s.range(of: "<[^>]+>", options:.regularExpression)
        while (r.location != NSNotFound) {
            s = s.replacingCharacters(in: r, with:"") as NSString
            r = s.range(of: "<[^>]+>", options:.regularExpression)
        }
        return s as String;

    }
    
    var stringByRemovingDuplicateNewLines: String {
        let components = self.components(separatedBy: CharacterSet.newlines)
        var processedComponenets = [String]()
        components.forEach({stringPiece in
            // The current piece is not empty, append it immediately.
            let stringPiece = stringPiece.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            if !stringPiece.isEmpty {
                // Remove empty pieces.
                processedComponenets.append(stringPiece)
            } else {
                // If the previous piece is not empty, append this empty piece.
                if let previousPiece = processedComponenets.last, !previousPiece.isEmpty {
                    processedComponenets.append(stringPiece)
                }
            }
        })
        // Combine pieces.
        return processedComponenets.joined(separator: "\n")
    }
    
    var httpForm: String {
        if self.hasPrefix("http:") || self.hasPrefix("https:") {
            return self
        } else {
            return "http:\(self)"
        }
    }
}
